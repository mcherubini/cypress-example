export const clickElement = (selector) => {
    cy.contains(selector).click()
}