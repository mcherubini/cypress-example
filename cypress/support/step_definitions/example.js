import { Given,And,When,Then } from "cypress-cucumber-preprocessor/steps";
import { clickElement } from './utils';

Given('I open Cypress page', () => {
  cy.visit('https://example.cypress.io')
})

When('I go to the commands page', () => {
  clickElement('type')
  cy.url().should('include', '/commands/actions')
})

And('I fill the email input', () => {
  cy.writeInput('.action-email','fake@email.com')
})

Then(`I see {string} in the input`, (input) => {
  cy.get('#email1')
    .focus()
      .should('have.value','fake@email.com')
})

/*describe('My First Test', () => {
    it('Gets, types and asserts', () => {
      cy.visit('https://example.cypress.io')
  
      cy.contains('type').click()
  
      // Should be on a new URL which includes '/commands/actions'
      cy.url().should('include', '/commands/actions')
  
      // Get an input, type into it and verify that the value has been updated
      cy.get('.action-email')
        .type('fake@email.com')
        .should('have.value', 'fake@email.com')
    })
  })*/